# DesafioStone

Desafio de Divisão de Lucros

## Baixando o repositório 

Pode-se baixar via [HTTPS](https://gitlab.com/rlbarros/desafio-stone.git) ou [GIT](git@gitlab.com:rlbarros/desafio-stone.git)


## Abrindo a Solução via Visual Studio

No Visual Studio Ir no Menu:

> Arquivo
>>Abrir
>>>Projeto ou Solução (Ctrl+Shift+O)

Ir na Pasta onde está o repositório e Abrir a Pasta *DesafioStone*

```
Abrir o arquivo DesafioStone.sln
```

##Considerações dos Padrões e Diretórios de Projeto

O desafio está composto em camadas, cada camada está em seu devido diretório no projeto

*A requisição HTTP navega entre as camadas do sistema, respeitando o Single Reponsibility Principle*

Seguindo o devido Fluxo:

> Cliente HTTP
>> Controllers
>>> Servicos
>>>> Repositórios
>
>>>Serviços
>
>>Controllers
>
> Cliente HTTP


As Interfaces nas camadas de Repositório e Serviço fomentam o Principio de Inversão de Dependencia e Diminui o acoplamento, 

Assim podemos utilizar da plataforma de Injeção de dependência do ASP.NET Core (Configurado no ConfigureServices do Startup.cs)
e também Mockar estas camadas para desenvovermos testes unitários. 

#### Properties
> Diretório padrão onde temos launchSettings.json

####Constants
>Diretório onde guardo o Valor do Salário Mínimo que é uma constante neste projeto

####Controllers
>Mapeia os Endpoints que formam a API Rest

#### Converter
>Abriga os conversores de DateTime (para entregar no formato yyyy-mm-dd)
>
> decimal (para entregar o valor formatado com R$ e separadores de decimal(,) e milhar(.))

#### Exception
> Abriga as exceções de validação de negócio que podem ocorrer
>>  Regras de Área de Atuação, Faixa Salarial ou Tempo de Admissão não Encontradas

>> Saldo disponibiliado insuficiente para a distribuição de lucros

#### Models
> Abriga as Classes de Modelo (Entidades) do Projeto

#### Repository
> Abriga as interfaces e classes de repositório que norteiam o acesso e armazenamento de dados


#### Seed
> Abriga o arquivo funcionarios.json dado com exemplo de massa de dados para o desafio.

#### Service
> Abriga o clases que executam a lógica de negócio da distribuição de lucros

> Abriga o servições padrão de CRUD da entidade Funcionario

#### Singleton
> Abriga instância únicas que servem toda a aplicação
>> Banco de Dados em Memória (Estão guardando as regras de pesos para a distribuição)
>
>> String de conexão do Firebase (cadastro de funcionários)

#### Utils
> Classes de Apoio para execução de tarefas específicas não funcionais.
>> Date Utils (Comparação de Datas )
>
>> Leitores e Formatadores de JSON
>
>> Padrão genérico de Banco de Dados em Memória.

### Executando o Projeto

Esperar o carregamento automático do projeto e das instalação das dependências Nuget


```
No canto inferior esquerdo, na barra de ações você irá observar o status: 'Pronto'
```

Após o Visual Studio Ir no Menu:

> Depúrar
>>Iniciar Depuração (F5)

ou 

>> Iniciar Sem Depurar (Ctrl+F5)


> Obs:Ao Inicializar a aplicação, os registros dos funcionários são atualizados no firebase.


### Entendendo os Endpoints e Utilizando o Swagger

Após executado o app .Net Core 5.0 ele automaticamente irá abrir no navegador esta URL

```
https://localhost:<PORTA>/swagger/index.html
```

Podemos Observar dois endpoints principais _Distribuição_ e _Funcionario_

#### Distribuição

Responsável por Calcular o Valor de Participação de Cada Funcionário.
De Acordo com as regras estabelecidas no desafio.

> URL: /api/Distribuicao?valor_disponibilizado
>> Valor Disponibilizado é um parâmetro numérico
>>> Quando o Valor Dispobilizado é maior  que o total distribuido aos funcionários o endpoint retorna OK (200)
> 
>>> Caso contrário o endpoint retornará um erro Bad Request (400) com uma mensagem de erro informando que o valor disponibilizado é menor que o total distribuido.

Pode-se executar via Swagger clicando em *Try It Out*, informando um valor (Ex: 5000000.00) e clicando em execute

OU utilizando diretamento o navegador de preferência e digitando na barra de endereço o url a seguir

```
https://localhost:<PORTA>/api/Distribuicao?valor_disponibilizado=5000000.00
```

#### Funcionário

Responsável por atualiar no Firebase o cadastro de Funcionários
nele temos as quatros operações básicas do CRUD (Create, Read, Update,Delete)

> URL: /api/Funcionario
>> Verbo GET
>>> Retorna todos os funcionários Cadastrados

>> Verbo POST
>>> Cria um novo registro de Funcionário (o json´é passado no corpo da requisição)

>> Verbo PUT
>>> Atualiza o registro de Funcionário (o json´é passado no corpo da requisição)

>> Verbo DELETE
>>> Remove um  registro de Funcionário (neste caso, o json precisa conter somente a propriedade matrícula preenchida)

### Roteiro de Testes

> passo 1: Conferência da Validação do Endpoint de Distribuição
>> 1.1 executar o endpoint de distribuição de lucros com o valor de cinco milhões de reais (5000000.00)
>>> conferir  o retorno do objeto distribuição e suas respectivas participações

>> 1.2: executar o endpoint de distribuição de lucros com o valor de quatro milhões de reais (4000000.00)
>>> conferir a mensagem de erro de saldo insuficiente.

> passo 2: Manipulando a listagem de funcionariose e efeitos na distribuição de lucros
>> 2.1 Crie um registro no endpoint funcionário no verbo POST

```
{
  "matricula": "0123456",
  "nome": "Rodrigo Lima Barros",
  "area": "Relacionamento com o Cliente",
  "cargo": "Costumer Success",
  "salario_bruto": "R$ 2.000,00",
  "data_de_admissao": "2020-12-12"
}
```

Pesos neste cenário:

    - 5 na Área de Atuação (Relacionamento com o Cliente)    
    - 1 no Salário (Abaixo de 3 salários mínimos)
    - 1 no Tempo de Admissão  (Abaixo de 1 ano) 

>> 2.2 Execute o endpoint de distribuição de lucros com cinco milhões  e confira as alterações de resultado nos calculos

>> 2.3 Altere o registro criado alterando data de admissão, salário, cargo e  área


```
{
  "matricula": "0123456",
  "nome": "Rodrigo Lima Barros",
  "area": "Tecnologia",
  "cargo": "Desenvolvedor de Software C# Sênior",
  "salario_bruto": "R$ 8.300,00",
  "data_de_admissao": "2011-12-12"
}
```

Pesos neste cenário:

    - 2 na Área de Atuação (Tecnologia)    
    - 3 no Salário (Acima de 5 sálarios mínimos e menor que 8 salários mínimos)
    - 5 no Tempo de Admissão  (Mais que 8 anos de casa)
    
>> 2.4 Execute o endpoint de distribuição de lucros com cinco milhões e confira as alterações de resultado nos calculos

>> 2.5 Exclua o registro no endpoint funcionário no verbo DELETE

```
{
  "matricula": "0123456"
}
```

>> 2.6 Execute o endpoint de distribuição de lucros com cinco milhões  e confira as alterações de resultado nos calculos


## Testes Unitários

### Considerações

Foram desenvolidos testes unitários para a parte de lógica de negócio do desafio proposto:

    - Conferência de Pesos para as Regras de Área de Atuação
        - Peso 1 Para Área de Diretoria
        - Peso 2 Para Área de Contabilidade, Financeiro e Tecnologia
        - Peso 3 Para Área de Serviços Gerais
        - Peso 5 Para Área de Relacionamento com o Cliente
    - Conferência de Pesos para as Regras de Faixa Salarial
        - Peso 1 Para Remuneração Até 3 Salários Mínimos
        - Peso 2 Para Remuneração entre 3 e 5 Salários Mínimos
        - Peso 3 Para Remuneração entre 5 e 8 Salários Mínimos
        - Peso 5 Para Remuneração acima de 8 Salários Mínimos
    - Conferência de Pesos para as Regras de Tempo de Admissão
        - Peso 1 Para Até 1 ano de casa
        - Peso 2 Para tempo de casa entre 1 e 3 anos.
        - Peso 3 Para tempo de casa entre 3 e 8 anos.
        - Peso 5 Para acima de 8 anos trabalhados
    -Conferência de Distribuição de Lucros
        - Contagem de Registros de Participações do Objecto Distribuição = 22
        - Verificação do Atributo TotalFuncionarios = 22
        - Soma dos Valores de Participação = R$ 4.544.926,18
        - Atributo Total Distribuido = R$ 4.544.926,18
        - Atributo SaldoTotalDisponibilizado = R$0,01
        - Verificação de exceção valor disponibilizado insuficiente 


### Execução dos Testes

No Visual Studio Ir no Menu:

> Testes
>>Gerenciador de Testes (Ctrl+E,T)
>>>Selecionar o Grupo Mâe DesafioStoneTests (20 testes unitários)
>
>>> Clicar no butão play para executar os testes (Ctrl +R,T)
>
>>> Observar os testes passando.


#Muito Obrigado!

[Linkedin](https://www.linkedin.com/in/rodrigo-lima-barros-48aa233a/)

