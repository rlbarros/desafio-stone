﻿using DesafioStone.Service;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DesafioStoneTests.Fixtures
{
    public class DistribuicaoServicoFixture
    {
        public IDistribuicaoServico DistribuicaoServico;

        public DistribuicaoServicoFixture()
        {
            DistribuicaoServico = new DistribuicaoServico(new FuncionarioServicoFixture().FuncionarioServico);
        }
    }
}
