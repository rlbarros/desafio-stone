﻿using DesafioStone.Constants;
using DesafioStone.Models;
using DesafioStone.Repository;
using DesafioStone.Service;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioStoneTests.Fixtures
{
    public class FuncionarioServicoFixture
    {
        public IFuncionarioServico FuncionarioServico;

        private Mock<IRepositorio<Funcionario>> _funcionarioRepositorio;

        private Mock<IServico<RegraAreaAtuacao>> _regraAreaAtuacaoServico;
        private Mock<IServico<RegraFaixaSalarial>> _regraFaixaSalarialServico;
        private Mock<IServico<RegraTempoAdmissao>> _regraTempoAdmissaoServico;

        public FuncionarioServicoFixture()
        {
            _funcionarioRepositorio = new Mock<IRepositorio<Funcionario>>();

            _regraAreaAtuacaoServico = new Mock<IServico<RegraAreaAtuacao>>();
            _regraFaixaSalarialServico = new Mock<IServico<RegraFaixaSalarial>>();
            _regraTempoAdmissaoServico = new Mock<IServico<RegraTempoAdmissao>>();

            FuncionarioServico = new FuncionarioServico(
                _funcionarioRepositorio.Object, 
                _regraAreaAtuacaoServico.Object,
                _regraFaixaSalarialServico.Object,
                _regraTempoAdmissaoServico.Object
            );

            Setup();
        }

        private void Setup()
        {
            SetupRegrasAreaAtuacaoServico();
            SetupRegrasFaixaSalarialServico();
            SetupRegrasTempoAdmissaoServico();
            SetupFuncionarioRepositorio();
        }

        private void SetupRegrasAreaAtuacaoServico()
        {
            IList<RegraAreaAtuacao> regrasAreaAtuacao = new List<RegraAreaAtuacao>()
            {
                new RegraAreaAtuacao()
                {
                    Setores = new string[] { "Diretoria" },
                    Peso = 1
                },
                new RegraAreaAtuacao()
                {
                    Setores = new string[] { "Contabilidade", "Financeiro", "Tecnologia" },
                    Peso = 2
                },
                new RegraAreaAtuacao()
                {
                    Setores = new string[] { "Serviços Gerais" },
                    Peso = 3
                },
                new RegraAreaAtuacao()
                {
                    Setores = new string[] { "Relacionamento com o Cliente" },
                    Peso = 5
                }
            };

            _regraAreaAtuacaoServico.Setup(s => s.Listar()).Returns(regrasAreaAtuacao);
        }

        private void SetupRegrasFaixaSalarialServico()
        {
            decimal tresSalariosMinimos = SalarioMinimo.VALOR * 3;
            decimal cincoSalariosMinimos = SalarioMinimo.VALOR * 5;
            decimal oitoSalariosMinimos = SalarioMinimo.VALOR * 8;

            IList<RegraFaixaSalarial> regrasFaixaSalarial = new List<RegraFaixaSalarial>()
            {
                new RegraFaixaSalarial()
                {
                    LimiteInferior = decimal.Zero,
                    LimiteSuperior = tresSalariosMinimos,
                    Peso = 1
                },
                new RegraFaixaSalarial()
                {
                    LimiteInferior = tresSalariosMinimos,
                    LimiteSuperior = cincoSalariosMinimos,
                    Peso = 2
                },
                new RegraFaixaSalarial()
                {
                    LimiteInferior = cincoSalariosMinimos,
                    LimiteSuperior = oitoSalariosMinimos,
                    Peso = 3
                },
                new RegraFaixaSalarial()
                {
                    LimiteInferior = oitoSalariosMinimos,
                    LimiteSuperior = decimal.MaxValue,
                    Peso = 5
                }
            };

            _regraFaixaSalarialServico.Setup(s => s.Listar()).Returns(regrasFaixaSalarial);
        }

        private void SetupRegrasTempoAdmissaoServico()
        {
            decimal tres = 3m;
            decimal oito = 8m;

            IList<RegraTempoAdmissao> regrasTempoAdmissao = new List<RegraTempoAdmissao>()
            {
                new RegraTempoAdmissao()
                {
                    AnosMinimosTrabalhados = decimal.Zero,
                    AnosMaximosTrabalhados = decimal.One,
                    Peso = 1
                },
                new RegraTempoAdmissao()
                {
                    AnosMinimosTrabalhados = decimal.One,
                    AnosMaximosTrabalhados = tres,
                    Peso = 2
                },
                new RegraTempoAdmissao()
                {
                    AnosMinimosTrabalhados = tres,
                    AnosMaximosTrabalhados = oito,
                    Peso = 3
                },
                new RegraTempoAdmissao()
                {
                    AnosMinimosTrabalhados = oito,
                    AnosMaximosTrabalhados = decimal.MaxValue,
                    Peso = 5
                }
            };

            _regraTempoAdmissaoServico.Setup(s => s.Listar()).Returns(regrasTempoAdmissao);
        }

        private void SetupFuncionarioRepositorio()
        {
            _funcionarioRepositorio.Setup(s => s.EncontrarTodos()).Returns(FuncionarioRepositorio.ObterFuncionariosDeInicializacao());
        }
    }
}
