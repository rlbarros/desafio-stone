﻿using DesafioStone.Constants;
using DesafioStone.Models;
using DesafioStone.Service;
using DesafioStoneTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DesafioStoneTests.Tests
{
    [Collection("Funcionario Servico collection")]
    public class RegraFaixaSalarialTest
    {
        private FuncionarioServicoFixture _funcionarioServicoFixture;
        private IFuncionarioServico _funcionarioServico;

        public RegraFaixaSalarialTest(FuncionarioServicoFixture funcionarioServicoFixture)
        {
            _funcionarioServicoFixture = funcionarioServicoFixture;
            _funcionarioServico = _funcionarioServicoFixture.FuncionarioServico;
        }

        private void GeneralTest(decimal pesoEsperado, decimal salarioInformado)
        {
            Assert.Equal(pesoEsperado, _funcionarioServico.ObterRegraFaixaSalarialDoFuncionario(
                new Funcionario()
                {
                    SalarioBruto = salarioInformado
                }
            ).Peso);
        }

        [Fact]
        public void Peso1_Ate3SalariosMinimos()
        {
            GeneralTest(1, SalarioMinimo.VALOR);
        }

        [Fact]
        public void Peso2_AcimaDe3Ate5SalariosMinimos()
        {
            GeneralTest(2, ((SalarioMinimo.VALOR*3)+0.01m));
        }

        [Fact]
        public void Peso3_AcimaDe5Ate8SalariosMinimos()
        {
            GeneralTest(3, ((SalarioMinimo.VALOR * 5) + 0.01m));
        }

        [Fact]
        public void Peso5_AcimaDe8SalariosMinimos()
        {
            GeneralTest(5, ((SalarioMinimo.VALOR * 8) + 0.01m));
        }
    }
}
