using DesafioStone.Models;
using DesafioStone.Service;
using DesafioStoneTests.Fixtures;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;

namespace DesafioStoneTests.Tests
{
    [Collection("Funcionario Servico collection")]
    public class RegraAreaDeAtuacaoTest
    {
        private FuncionarioServicoFixture _funcionarioServicoFixture;
        private IFuncionarioServico _funcionarioServico;

        public RegraAreaDeAtuacaoTest(FuncionarioServicoFixture funcionarioServicoFixture)
        {
            _funcionarioServicoFixture = funcionarioServicoFixture;
            _funcionarioServico = _funcionarioServicoFixture.FuncionarioServico;
        }

       private void GeneralTest(decimal pesoEsperado, string areaInformada)
        {
            Assert.Equal(pesoEsperado, _funcionarioServico.ObterRegraAreaAtuacaoDoFuncionario(
                new Funcionario()
                {
                    Area = areaInformada
                }
            ).Peso);
        }

        [Fact]
        public void Peso1_Diretoria()
        {
            GeneralTest(1, "Diretoria");
        }

        [Fact]
        public void Peso2_Contabilidade()
        {
            GeneralTest(2, "Contabilidade");
        }

        [Fact]
        public void Peso2_Financeiro()
        {
            GeneralTest(2, "Financeiro");
        }

        [Fact]
        public void Peso2_Tecnologia()
        {
            GeneralTest(2, "Tecnologia");
        }

        [Fact]
        public void Peso3_ServicosGerais()
        {
            GeneralTest(3, "Servi�os Gerais");
        }

        [Fact]
        public void Peso5_RelacionamentoComCliente()
        {
            GeneralTest(5, "Relacionamento com o Cliente");
        }
    }
}
