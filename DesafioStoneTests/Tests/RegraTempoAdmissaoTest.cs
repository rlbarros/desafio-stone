﻿using DesafioStone.Models;
using DesafioStone.Service;
using DesafioStoneTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DesafioStoneTests.Tests
{
    [Collection("Funcionario Servico collection")]
    public class RegraTempoAdmissaoTest
    {
        private FuncionarioServicoFixture _funcionarioServicoFixture;
        private IFuncionarioServico _funcionarioServico;

        public RegraTempoAdmissaoTest(FuncionarioServicoFixture funcionarioServicoFixture)
        {
            _funcionarioServicoFixture = funcionarioServicoFixture;
            _funcionarioServico = _funcionarioServicoFixture.FuncionarioServico;
        }

        private void GeneralTest(decimal pesoEsperado, DateTime dataAdmissao)
        {
            Assert.Equal(pesoEsperado, _funcionarioServico.ObterRegraTempoAdmissaoDoFuncionario(
                new Funcionario()
                {
                    DataAdmissao = dataAdmissao
                }
            ).Peso);
        }

        [Fact]
        public void Peso1_Ate1AnoTrabalhado()
        {
            GeneralTest(1, DateTime.Now.AddMonths(-11).AddDays(-30));
        }

        [Fact]
        public void Peso2_AcimaDe1Ate3AnosTrabalhados()
        {
            GeneralTest(2, DateTime.Now.AddYears(-1));
        }

        [Fact]
        public void Peso3_AcimaDe3Ate8AnosTrabalhados()
        {
            GeneralTest(3, DateTime.Now.AddYears(-3));
        }

        [Fact]
        public void Peso5_AcimaDe8AnosTrabalhados()
        {
            GeneralTest(5, DateTime.Now.AddYears(-8));
        }
    }
}
