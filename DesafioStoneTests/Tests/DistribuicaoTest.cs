﻿using DesafioStone.Exception;
using DesafioStone.Models;
using DesafioStone.Service;
using DesafioStoneTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DesafioStoneTests.Tests
{
    [Collection("Distribuicao Servico collection")]
    public class DistribuicaoTest
    {
        private DistribuicaoServicoFixture _distribuicaoServicoFixture;
        private IDistribuicaoServico _distribuicaoServico;
        private Distribuicao _distribuicao;
        public DistribuicaoTest(DistribuicaoServicoFixture distribuicaoServicoFixture)
        {
            _distribuicaoServicoFixture = distribuicaoServicoFixture;
            _distribuicaoServico = _distribuicaoServicoFixture.DistribuicaoServico;
            _distribuicao = _distribuicaoServico.DistribuirLucros(4544926.19m);
        }

        [Fact]
        public void LancaExcecaoValorDisponibilizadoInsuficiente()
        {
            Assert.Throws<ValorDisponibilizadoInsuficienteExcecao>(() => _distribuicaoServico.DistribuirLucros(4544926.17m));
        }

        [Fact]
        public void TotalFuncionarios_22()
        {
            Assert.Equal(22, _distribuicao.TotalFuncionarios);
        }

        [Fact]
        public void ContagemParticapacoes_22()
        {
            Assert.Equal(22, _distribuicao.Participacoes.Count());
        }

        [Fact]
        public void TotalDistribuido_4544926_18()
        {
            Assert.Equal(4544926.18m, _distribuicao.TotalDistribuido);
        }

        [Fact]
        public void SomaParticipacoes_4544926_18()
        {
            Assert.Equal(4544926.18m, _distribuicao.Participacoes.Sum(item => item.ValorParticipacao));
        }

        [Fact]
        public void SaldoTotalDisponibilizado_0_01()
        {
            Assert.Equal(0.01m, _distribuicao.SaldoTotalDisponibilizado);
        }
    }
}
