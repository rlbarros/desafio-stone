﻿using DesafioStoneTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DesafioStoneTests.Collections
{
    [CollectionDefinition("Funcionario Servico collection")]
    public class FuncionarioServicoCollection : ICollectionFixture<FuncionarioServicoFixture>
    {
    }
}
