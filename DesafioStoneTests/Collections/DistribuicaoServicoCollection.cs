﻿using DesafioStoneTests.Fixtures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DesafioStoneTests.Collections
{
    [CollectionDefinition("Distribuicao Servico collection")]
    public class DistribuicaoServicoCollection : ICollectionFixture<DistribuicaoServicoFixture>
    {
    }
}
