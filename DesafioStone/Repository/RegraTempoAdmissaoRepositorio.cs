﻿using DesafioStone.Models;
using DesafioStone.Singleton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Repository
{
    public class RegraTempoAdmissaoRepositorio : IRepositorioInicializavel<RegraTempoAdmissao>
    {
        public bool Adicionar(RegraTempoAdmissao registro)
        {
            RepositoriosEmMemoria.REGRAS_TEMPOS_ADMISSAO.records.Add(registro);
            return true;
        }

        public bool Alterar(RegraTempoAdmissao registro)
        {
            Apagar(registro);
            return Adicionar(registro);
        }

        public bool Apagar(RegraTempoAdmissao registro)
        {
            RepositoriosEmMemoria.REGRAS_TEMPOS_ADMISSAO.records.Remove(registro);
            return true;
        }

        public void Inicializar()
        {
            decimal tres = 3m;
            decimal oito = 8m;
            Adicionar(new RegraTempoAdmissao()
            {
                AnosMinimosTrabalhados = decimal.Zero,
                AnosMaximosTrabalhados = decimal.One,
                Peso = 1
            });
            Adicionar(new RegraTempoAdmissao()
            {
                AnosMinimosTrabalhados = decimal.One,
                AnosMaximosTrabalhados = tres,
                Peso = 2
            });
            Adicionar(new RegraTempoAdmissao()
            {
                AnosMinimosTrabalhados = tres,
                AnosMaximosTrabalhados = oito,
                Peso = 3
            });
            Adicionar(new RegraTempoAdmissao()
            {
                AnosMinimosTrabalhados = oito,
                AnosMaximosTrabalhados = decimal.MaxValue,
                Peso = 5
            });
        }

        public IEnumerable<RegraTempoAdmissao> EncontrarTodos()
        {
            return RepositoriosEmMemoria.REGRAS_TEMPOS_ADMISSAO.records;
        }
    }
}
