﻿using DesafioStone.Constants;
using DesafioStone.Models;
using DesafioStone.Singleton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Repository
{
    public class RegraFaixaSalarialRepositorio : IRepositorioInicializavel<RegraFaixaSalarial>
    {
        public bool Adicionar(RegraFaixaSalarial registro)
        {
            RepositoriosEmMemoria.REGRAS_SALARIOS.records.Add(registro);
            return true;
        }

        public bool Alterar(RegraFaixaSalarial registro)
        {
            Apagar(registro);
            return Adicionar(registro);
        }

        public bool Apagar(RegraFaixaSalarial registro)
        {
            RepositoriosEmMemoria.REGRAS_SALARIOS.records.Remove(registro);
            return true;
        }

        public void Inicializar()
        {
            decimal tresSalariosMinimos = SalarioMinimo.VALOR * 3;
            decimal cincoSalariosMinimos = SalarioMinimo.VALOR * 5;
            decimal oitoSalariosMinimos = SalarioMinimo.VALOR * 8;
            Adicionar(new RegraFaixaSalarial()
            {
                LimiteInferior = decimal.Zero,
                LimiteSuperior = tresSalariosMinimos,
                Peso = 1
            });
            Adicionar(new RegraFaixaSalarial()
            {
                LimiteInferior = tresSalariosMinimos,
                LimiteSuperior = cincoSalariosMinimos,
                Peso = 2
            });
            Adicionar(new RegraFaixaSalarial()
            {
                LimiteInferior = cincoSalariosMinimos,
                LimiteSuperior = oitoSalariosMinimos,
                Peso = 3
            });
            Adicionar(new RegraFaixaSalarial()
            {
                LimiteInferior = oitoSalariosMinimos,
                LimiteSuperior = decimal.MaxValue,
                Peso = 5
            });
        }

        public IEnumerable<RegraFaixaSalarial> EncontrarTodos()
        {
            return RepositoriosEmMemoria.REGRAS_SALARIOS.records;
        }
    }
}
