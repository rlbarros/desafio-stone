﻿using DesafioStone.Models;
using DesafioStone.Singleton;
using DesafioStone.Utils;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DesafioStone.Repository
{
    public class FuncionarioRepositorio : IRepositorioInicializavel<Funcionario>
    {
        private readonly HttpClient _client;

        private const string funcionarios = "funcionarios.json";
        private string uri = Firebase.Connection + funcionarios;

        public FuncionarioRepositorio()
        {
            _client = new HttpClient();

            InitClient();
        }

        private void InitClient()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        private string FuncionarioURI(Funcionario funcionario)
        {
            return $@"{uri.Replace(".json", "")}/{funcionario.Matricula}.json";
        }

        private bool ProcessarMensagemDeResposta(HttpResponseMessage response)
        {
            if (response.StatusCode.Equals(HttpStatusCode.OK))
                return true;
            else
            {
                Task<string> contentTask = Task.Run<string>(async () => await response.Content.ReadAsStringAsync());
                string content = contentTask.Result;
                throw new System.Exception(content);
            }
        }

        private bool ProcessarMensagemDeResposta(Task<HttpResponseMessage> responseTask)
        {
            HttpResponseMessage response = responseTask.Result;
            return ProcessarMensagemDeResposta(response);
        }
    
        public bool Adicionar(Funcionario registro)
        {
            return Alterar(registro);
        }

        public bool Alterar(Funcionario registro)
        {
            Task<HttpResponseMessage> responseTask = Task.Run<HttpResponseMessage>(async () => await _client.PutAsync(FuncionarioURI(registro), new StringContent(FormatJSON(registro), Encoding.UTF8, "application/json")));

            return ProcessarMensagemDeResposta(responseTask);
        }

        public bool Apagar(Funcionario registro)
        {
            Task<HttpResponseMessage> responseTask = Task.Run<HttpResponseMessage>(async () => await _client.DeleteAsync(FuncionarioURI(registro)));

            return ProcessarMensagemDeResposta(responseTask);
        }

        public IEnumerable<Funcionario> EncontrarTodos()
        {
            Task<HttpResponseMessage> responseTask = Task.Run<HttpResponseMessage>(async () => await _client.GetAsync(uri));
            HttpResponseMessage response = responseTask.Result;
            
            Task<string> contentTask = Task.Run<string>(async () => await response.Content.ReadAsStringAsync()); 
            string content = contentTask.Result;

            return ParseJSON(content);
        }

        public static IEnumerable<Funcionario> ObterFuncionariosDeInicializacao()
        {
            return ParseJSON(JSONReader.Read(funcionarios));
        }

        private static IEnumerable<Funcionario> ParseJSON(string content)
        {
            IList<Funcionario> funcionarios = new List<Funcionario>();

            JObject jObject = JObject.Parse(content);
            IEnumerable<JToken> jFuncionarios = jObject.Children();
            foreach (JToken jFuncionario in jFuncionarios)
            {
                Funcionario funcionario = new Funcionario();
                foreach (JToken jPropriedade in jFuncionario.Children())
                {
                    funcionario.Matricula = jPropriedade.Value<string>("matricula");
                    funcionario.Nome = jPropriedade.Value<string>("nome");
                    funcionario.Area = jPropriedade.Value<string>("area");
                    funcionario.Cargo = jPropriedade.Value<string>("cargo");
                    funcionario.SalarioBruto = JSONReader.ParseMoney(jPropriedade.Value<string>("salario_bruto"));
                    funcionario.DataAdmissao = JSONReader.ParseDate(jPropriedade.Value<string>("data_de_admissao"));
                }
                funcionarios.Add(funcionario);
            }

            return funcionarios;
        }

        private static string FormatJSON(Funcionario funcionario)
        {
            JObject jObject = new JObject();

            jObject.Add("matricula", funcionario.Matricula);
            jObject.Add("nome", funcionario.Nome);
            jObject.Add("area", funcionario.Area);
            jObject.Add("cargo", funcionario.Cargo);
            jObject.Add("salario_bruto", JSONFormatter.FormatMoney(funcionario.SalarioBruto));
            jObject.Add("data_de_admissao", JSONFormatter.FormatDate(funcionario.DataAdmissao));

            return jObject.ToString();
        }

        public async void Inicializar()
        {
            HttpResponseMessage response = await _client.PutAsync(uri, new StringContent(JSONReader.Read(funcionarios), Encoding.UTF8, "application/json"));
            _ = ProcessarMensagemDeResposta(response);
        }
    }
}
