﻿using DesafioStone.Models;
using DesafioStone.Singleton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Repository
{
    public class RegraAreaAtuacaoRepositorio : IRepositorioInicializavel<RegraAreaAtuacao>
    {
        public bool Adicionar(RegraAreaAtuacao registro)
        {
            RepositoriosEmMemoria.REGRAS_ATUACOES.records.Add(registro);
            return true;
        }

        public bool Alterar(RegraAreaAtuacao registro)
        {
            Apagar(registro);
            return Adicionar(registro);
        }

        public bool Apagar(RegraAreaAtuacao registro)
        {
            RepositoriosEmMemoria.REGRAS_ATUACOES.records.Remove(registro);
            return true;
        }

        public void Inicializar()
        {
            Adicionar(new RegraAreaAtuacao() {
                Setores = new string[] { "Diretoria" },
                Peso = 1
            });
            Adicionar(new RegraAreaAtuacao() {
                Setores = new string[] { "Contabilidade", "Financeiro", "Tecnologia" },
                Peso = 2
            });
            Adicionar(new RegraAreaAtuacao() { 
                Setores = new string[] { "Serviços Gerais" },
                Peso = 3
            });
            Adicionar(new RegraAreaAtuacao()
            {
                Setores = new string[] { "Relacionamento com o Cliente" },
                Peso = 5
            });
        }

        public IEnumerable<RegraAreaAtuacao> EncontrarTodos()
        {
            return RepositoriosEmMemoria.REGRAS_ATUACOES.records;
        }
    }
}
