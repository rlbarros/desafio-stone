﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Repository
{
    public interface IRepositorioInicializavel<T> : IRepositorio<T>
    {
        void Inicializar();
    }
}
