﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Repository
{
    public interface IRepositorio<R>
    {
        bool Adicionar(R registro);
        bool Alterar(R registro);
        bool Apagar(R registro);
        IEnumerable<R> EncontrarTodos();
    }
}
