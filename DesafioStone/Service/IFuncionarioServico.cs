﻿using DesafioStone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public interface IFuncionarioServico : IServico<Funcionario>
    {
        RegraAreaAtuacao ObterRegraAreaAtuacaoDoFuncionario(Funcionario funcionario);
        RegraFaixaSalarial ObterRegraFaixaSalarialDoFuncionario(Funcionario funcionario);
        RegraTempoAdmissao ObterRegraTempoAdmissaoDoFuncionario(Funcionario funcionario);
    }
}
