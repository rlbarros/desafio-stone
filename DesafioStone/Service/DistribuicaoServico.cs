﻿using DesafioStone.Exception;
using DesafioStone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public class DistribuicaoServico : IDistribuicaoServico
    {

        private readonly IFuncionarioServico _funcionarioServico;

        public DistribuicaoServico(IFuncionarioServico funcionarioServico)
        {
            _funcionarioServico = funcionarioServico;
        }

        public Distribuicao DistribuirLucros(decimal valorDisponibilizado)
        {

            List<Participacao> participacoes = new List<Participacao>();

            foreach (Funcionario funcionario in _funcionarioServico.Listar())
            {
                RegraAreaAtuacao regraAreaDeAtuacao = _funcionarioServico.ObterRegraAreaAtuacaoDoFuncionario(funcionario);
                RegraFaixaSalarial regraFaixaSalarial = _funcionarioServico.ObterRegraFaixaSalarialDoFuncionario(funcionario);
                RegraTempoAdmissao regraTempoAdmissao = _funcionarioServico.ObterRegraTempoAdmissaoDoFuncionario(funcionario);

                participacoes.Add(new Participacao()
                {
                    Matricula = funcionario.Matricula,
                    Nome = funcionario.Nome,
                    ValorParticipacao = CalcularBonusFuncionario(funcionario, regraAreaDeAtuacao, regraFaixaSalarial, regraTempoAdmissao)
                });
            }

            Distribuicao distribuicao = new Distribuicao()
            {
                TotalDisponibilizado = valorDisponibilizado,
                Participacoes = participacoes
            };

            if (distribuicao.TotalDisponibilizado < distribuicao.TotalDistribuido)
            {
                throw new ValorDisponibilizadoInsuficienteExcecao(distribuicao.TotalDisponibilizado, distribuicao.TotalDistribuido);
            }

            return distribuicao;
        }

        private decimal CalcularBonusFuncionario(
            Funcionario funcionario,
            RegraAreaAtuacao regraAreaDeAtuacao,
            RegraFaixaSalarial regraFaixaSalarial,
            RegraTempoAdmissao regraTempoAdmissao)
        {
            decimal salarioBruto = funcionario.SalarioBruto;
            decimal mesesDoAno = 12m;
            decimal bonus = ((salarioBruto * regraTempoAdmissao.Peso) + (salarioBruto * regraAreaDeAtuacao.Peso) / regraFaixaSalarial.Peso) * mesesDoAno;
            return Math.Round(bonus, 2, MidpointRounding.AwayFromZero);
        }
    }
}
