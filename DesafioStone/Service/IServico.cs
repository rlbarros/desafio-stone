﻿using DesafioStone.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public interface IServico<M> 
    {
        bool Criar(M modelo);
        bool Atualizar(M modelo);
        bool Remover(M modelo);
        IEnumerable<M> Listar();
    }
}
