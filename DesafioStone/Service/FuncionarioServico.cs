﻿using DesafioStone.Exception;
using DesafioStone.Models;
using DesafioStone.Repository;
using DesafioStone.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public class FuncionarioServico : IFuncionarioServico
    {
        private readonly IRepositorio<Funcionario> _repositorio;
        private readonly IServico<RegraAreaAtuacao> _regraAreaDeAtuacaoServico;
        private readonly IServico<RegraFaixaSalarial> _regraFaixaSalarialServico;
        private readonly IServico<RegraTempoAdmissao> _regraTempoAdmissaoServico;

        public FuncionarioServico(
            IRepositorio<Funcionario> repositorio,
            IServico<RegraAreaAtuacao> regraAreaDeAtuacaoServico,
            IServico<RegraFaixaSalarial> regraFaixaSalarialServico,
            IServico<RegraTempoAdmissao> regraTempoAdmissaoServico)
        {
            _repositorio = repositorio;
            _regraAreaDeAtuacaoServico = regraAreaDeAtuacaoServico;
            _regraFaixaSalarialServico = regraFaixaSalarialServico;
            _regraTempoAdmissaoServico = regraTempoAdmissaoServico;
        }

        public bool Atualizar(Funcionario modelo)
        {
            return _repositorio.Alterar(modelo);
        }

        public bool Criar(Funcionario modelo)
        {
            return _repositorio.Adicionar(modelo);
        }

        public IEnumerable<Funcionario> Listar()
        {
            return _repositorio.EncontrarTodos();
        }

        public RegraAreaAtuacao ObterRegraAreaAtuacaoDoFuncionario(Funcionario funcionario)
        {
            foreach (RegraAreaAtuacao regraAreaAtuacao in _regraAreaDeAtuacaoServico.Listar())
            {
                foreach (String setor in regraAreaAtuacao.Setores)
                {
                    if (setor.Equals(funcionario.Area))
                        return regraAreaAtuacao;
                }
            }
            throw new RegraAreaAtuacaoNaoEncontradaExcecao(funcionario.Area);
        }

        public RegraFaixaSalarial ObterRegraFaixaSalarialDoFuncionario(Funcionario funcionario)
        {
            decimal salarioBruto = funcionario.SalarioBruto;
            foreach (RegraFaixaSalarial regraFaixaSalarial in _regraFaixaSalarialServico.Listar())
            {
                if (salarioBruto > regraFaixaSalarial.LimiteInferior && salarioBruto <= regraFaixaSalarial.LimiteSuperior)
                    return regraFaixaSalarial;
            }
            throw new RegraFaixaSalarialNaoEncontradaExcecao(salarioBruto);
        }

        public RegraTempoAdmissao ObterRegraTempoAdmissaoDoFuncionario(Funcionario funcionario)
        {
            int tempoDecorrido = DateUtils.YearsFrom(funcionario.DataAdmissao.Date, DateTime.Now.Date);
            foreach (RegraTempoAdmissao regraTempoAdmissao in _regraTempoAdmissaoServico.Listar())
            {
                if (tempoDecorrido >= regraTempoAdmissao.AnosMinimosTrabalhados && tempoDecorrido < regraTempoAdmissao.AnosMaximosTrabalhados)
                    return regraTempoAdmissao;
            }
            throw new RegraTempoAdmissaoNaoEncontradoExcecao(tempoDecorrido);
        }

        public bool Remover(Funcionario modelo)
        {
            return _repositorio.Apagar(modelo);
        }
    }
}
