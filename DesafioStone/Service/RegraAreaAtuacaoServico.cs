﻿using DesafioStone.Models;
using DesafioStone.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public class RegraAreaAtuacaoServico : IServico<RegraAreaAtuacao>
    {
        private readonly IRepositorio<RegraAreaAtuacao> _repositorio;

        public RegraAreaAtuacaoServico(IRepositorio<RegraAreaAtuacao> repositorio)
        {
            _repositorio = repositorio;
        }
        public bool Atualizar(RegraAreaAtuacao modelo)
        {
            return _repositorio.Alterar(modelo);
        }

        public bool Criar(RegraAreaAtuacao modelo)
        {
            return _repositorio.Adicionar(modelo);
        }

        public IEnumerable<RegraAreaAtuacao> Listar()
        {
            return _repositorio.EncontrarTodos();
        }

        public bool Remover(RegraAreaAtuacao modelo)
        {
            return _repositorio.Apagar(modelo);
        }
    }
}
