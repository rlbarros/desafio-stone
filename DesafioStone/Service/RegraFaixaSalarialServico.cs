﻿using DesafioStone.Models;
using DesafioStone.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public class RegraFaixaSalarialServico : IServico<RegraFaixaSalarial>
    {
        private readonly IRepositorio<RegraFaixaSalarial> _repositorio;

        public RegraFaixaSalarialServico(IRepositorio<RegraFaixaSalarial> repositorio)
        {
            _repositorio = repositorio;
        }
        public bool Atualizar(RegraFaixaSalarial modelo)
        {
            return _repositorio.Alterar(modelo);
        }

        public bool Criar(RegraFaixaSalarial modelo)
        {
            return _repositorio.Adicionar(modelo);
        }

        public IEnumerable<RegraFaixaSalarial> Listar()
        {
            return _repositorio.EncontrarTodos();
        }

        public bool Remover(RegraFaixaSalarial modelo)
        {
            return _repositorio.Apagar(modelo);
        }
    }
}
