﻿using DesafioStone.Models;
using DesafioStone.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public class RegraTempoAdmissaoServico : IServico<RegraTempoAdmissao>
    {
        private readonly IRepositorio<RegraTempoAdmissao> _repositorio;

        public RegraTempoAdmissaoServico(IRepositorio<RegraTempoAdmissao> repositorio)
        {
            _repositorio = repositorio;
        }
        public bool Atualizar(RegraTempoAdmissao modelo)
        {
            return _repositorio.Alterar(modelo);
        }

        public bool Criar(RegraTempoAdmissao modelo)
        {
            return _repositorio.Adicionar(modelo);
        }

        public IEnumerable<RegraTempoAdmissao> Listar()
        {
            return _repositorio.EncontrarTodos();
        }

        public bool Remover(RegraTempoAdmissao modelo)
        {
            return _repositorio.Apagar(modelo);
        }
    }
}
