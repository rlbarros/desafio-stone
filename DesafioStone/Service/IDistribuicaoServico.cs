﻿using DesafioStone.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Service
{
    public interface IDistribuicaoServico
    {
        Distribuicao DistribuirLucros(decimal valorDisponiblilizado);
    }
}
