﻿using DesafioStone.Models;
using DesafioStone.Service;
using DesafioStone.Singleton;
using DesafioStone.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DesafioStone.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DistribuicaoController : Controller
    {
        private readonly IDistribuicaoServico _distribuicaoServico;


        public DistribuicaoController(IDistribuicaoServico distribuicaoServico)
        {
            _distribuicaoServico = distribuicaoServico;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Distribuicao))]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Get([FromQuery(Name = "valor_disponibilizado")] decimal valorDisponiblilizado)
        {
            try
            {
                Distribuicao distribuicao = _distribuicaoServico.DistribuirLucros(valorDisponiblilizado);
                return new OkObjectResult(distribuicao);
            }
            catch (System.Exception ex)
            {
                return new BadRequestObjectResult(ex.Message);
            }
        }
    }
}
