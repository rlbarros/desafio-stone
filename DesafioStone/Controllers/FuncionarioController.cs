﻿using DesafioStone.Models;
using DesafioStone.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FuncionarioController : Controller
    {
        private readonly IFuncionarioServico _funcionarioServico;

        public FuncionarioController(IFuncionarioServico funcionarioServico)
        {
            _funcionarioServico = funcionarioServico;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<Funcionario>))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Get()
        {
            try
            {
                IEnumerable<Funcionario> funcionarios = _funcionarioServico.Listar();
                return new OkObjectResult(funcionarios);
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message); 
            }
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Post([FromBody] Funcionario funcionario)
        {
            try
            {
                bool ok = _funcionarioServico.Criar(funcionario);
                if (ok)
                    return new CreatedResult("funcionarios", ok);
                else
                    throw new System.Exception("Impossível Incluir Funcionário, tente novamente posteriormente.");
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Put([FromBody] Funcionario funcionario)
        {
            try
            {
                bool ok = _funcionarioServico.Atualizar(funcionario);
                if (ok)
                    return new CreatedResult("funcionarios", ok);
                else
                    throw new System.Exception("Impossível Alterar Funcionário, tente novamente posteriormente.");
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        [HttpDelete]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(bool))]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public IActionResult Delete([FromBody] Funcionario funcionario)
        {
            try
            {
                bool ok = _funcionarioServico.Remover(funcionario);
                if (ok)
                    return new OkObjectResult(ok);
                else
                    throw new System.Exception("Impossível Deletar Funcionário, tente novamente posteriormente.");
            }
            catch (System.Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
