﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DesafioStone.Models
{
    public class Distribuicao
    {
        [JsonPropertyName("participacoes")]
        public IEnumerable<Participacao> Participacoes { get; set; }
        [JsonPropertyName("total_de_funcionarios")]
        public int TotalFuncionarios => Participacoes.Count();
        [JsonPropertyName("total_distribuido")]
        public decimal TotalDistribuido => Participacoes.Sum(participacao => participacao.ValorParticipacao);
        [JsonPropertyName("total_disponibilizado")]
        public decimal TotalDisponibilizado { get; set; }
        [JsonPropertyName("saldo_total_disponibilizado")]
        public decimal SaldoTotalDisponibilizado => TotalDisponibilizado - TotalDistribuido;


    }
}
