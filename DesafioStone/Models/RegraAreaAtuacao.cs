﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Models
{
    public class RegraAreaAtuacao
    {
        public string[] Setores { get; set; }
        public decimal Peso { get; set; }
    }
}
