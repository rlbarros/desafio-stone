﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DesafioStone.Models
{
    public class Participacao
    {
        [JsonPropertyName("matricula")]
        public string Matricula { get; set; }
        [JsonPropertyName("nome")]
        public string Nome { get; set; }
        [JsonPropertyName("valor_da_participacao")]
        public decimal ValorParticipacao { get; set; }
    }
}
