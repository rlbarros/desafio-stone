﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Models
{
    public class RegraTempoAdmissao
    {
        public decimal AnosMinimosTrabalhados { get; set; }
        public decimal AnosMaximosTrabalhados { get; set; }
        public decimal Peso { get; set; }
    }
}
