﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace DesafioStone.Models
{
    public class Funcionario
    {
        [JsonPropertyName("matricula")]
        public string Matricula { get; set; }
        [JsonPropertyName("nome")]
        public string Nome { get; set; }
        [JsonPropertyName("area")]
        public string Area { get; set; }
        [JsonPropertyName("cargo")]
        public string Cargo { get; set; }
        [JsonPropertyName("salario_bruto")]
        public decimal SalarioBruto { get; set; }
        [JsonPropertyName("data_de_admissao")]
        public DateTime DataAdmissao { get; set; }
    }
}
