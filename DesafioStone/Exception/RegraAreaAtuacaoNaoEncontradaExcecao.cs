﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Exception
{
    public class RegraAreaAtuacaoNaoEncontradaExcecao : System.Exception
    {
        private string _setorProcurado;

        public RegraAreaAtuacaoNaoEncontradaExcecao(string setorProcurado)
        {
            this._setorProcurado = setorProcurado;
        }
        public override string Message => $"O Setor {_setorProcurado} está sem associação de Regra de Área de Atuação";
    }
}
