﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Exception
{
    public class RegraFaixaSalarialNaoEncontradaExcecao : System.Exception
    {
        private decimal _salarioProcurado;

        public RegraFaixaSalarialNaoEncontradaExcecao(decimal salarioProcurado)
        {
            this._salarioProcurado = salarioProcurado;
        }

        public override string Message => $"O salário {_salarioProcurado.ToString("C2")} está sem associação de Regra de Faixa Salarial";
    }
}
