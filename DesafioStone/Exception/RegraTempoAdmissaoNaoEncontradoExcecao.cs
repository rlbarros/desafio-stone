﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Exception
{
    public class RegraTempoAdmissaoNaoEncontradoExcecao : System.Exception
    {
        private decimal _tempoDecorridoProcurado;

        public RegraTempoAdmissaoNaoEncontradoExcecao(decimal tempoDecorridoProcurado)
        {
            this._tempoDecorridoProcurado = tempoDecorridoProcurado;
        }

        public override string Message => $"O tempo { _tempoDecorridoProcurado } Ano(s) está sem associação de Regra de Tempo de Admissão";
    }
}
