﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Exception
{
    public class ValorDisponibilizadoInsuficienteExcecao : System.Exception
    {
        private decimal _valorDisponibilizado;
        private decimal _valorDistribuido;

        public ValorDisponibilizadoInsuficienteExcecao(decimal valorDisponibilizado, decimal valorDistribuido)
        {
            this._valorDisponibilizado = valorDisponibilizado;
            this._valorDistribuido = valorDistribuido;
        }

        public override string Message => $"O valor disponibilizado { _valorDisponibilizado.ToString("C2") } é " +
            $"insuficiente para suprir o valor Distribuido: {_valorDistribuido.ToString("C2")}";
    }
}
