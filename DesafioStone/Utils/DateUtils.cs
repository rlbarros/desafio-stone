﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Utils
{
    public class DateUtils
    {
        public static int YearsFrom(DateTime startDate, DateTime endDate)
        {
            return new DateTime(endDate.Subtract(startDate).Ticks).Year - 1;
        }
    }
}
