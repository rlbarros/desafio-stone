﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Utils
{
    public class JSONFormatter
    {
        public static string FormatMoney(decimal value)
        {
            return $"R$ {value.ToString("N2", CultureInfo.CreateSpecificCulture("es-ES"))}";
        }

        public static string FormatDate(DateTime value)
        {
            return value.ToString("yyyy-MM-dd");
        }
    }
}
