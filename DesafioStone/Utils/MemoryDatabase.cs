﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Utils
{
    public class MemoryDatabase<T>
    {
        public IList<T> records { get; }

        public MemoryDatabase()
        {
            this.records = new List<T>();
        }
    }
}
