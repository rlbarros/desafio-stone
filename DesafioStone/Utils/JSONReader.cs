﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioStone.Utils
{
    public class JSONReader
    {
        public static string Read(string fileName)
        {
            string dir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            string file = dir + @$"\Seed\{fileName}";

            using (StreamReader r = new StreamReader(file, Encoding.UTF8))
            {
                return r.ReadToEnd();
            }
        }

        public static decimal ParseMoney(string value)
        {
            value = value.Replace("R$", "").Replace(@".", "").Trim();
            return Convert.ToDecimal(value);
        }

        public static DateTime ParseDate(string value)
        {
            return DateTime.ParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
        }
    }
}
