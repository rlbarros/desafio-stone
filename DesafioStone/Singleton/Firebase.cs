﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Singleton
{
    public class Firebase
    {
        private static string _connection;

        public static string Connection { 
            get {
                return _connection;
            }
            set
            {
                if( _connection == null)
                {
                    _connection = value;
                }
            }
        }
    }
}
