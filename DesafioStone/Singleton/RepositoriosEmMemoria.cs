﻿using DesafioStone.Models;
using DesafioStone.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone.Singleton
{
    public class RepositoriosEmMemoria
    {
        static RepositoriosEmMemoria()
        {
            REGRAS_ATUACOES = new MemoryDatabase<RegraAreaAtuacao>();
            REGRAS_SALARIOS = new MemoryDatabase<RegraFaixaSalarial>();
            REGRAS_TEMPOS_ADMISSAO = new MemoryDatabase<RegraTempoAdmissao>();
        }

        public static MemoryDatabase<RegraAreaAtuacao> REGRAS_ATUACOES { get; }
        public static MemoryDatabase<RegraFaixaSalarial> REGRAS_SALARIOS { get; set; }
        public static MemoryDatabase<RegraTempoAdmissao> REGRAS_TEMPOS_ADMISSAO { get; set; }
}
}
