using DesafioStone.Converter;
using DesafioStone.Models;
using DesafioStone.Repository;
using DesafioStone.Service;
using DesafioStone.Singleton;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioStone
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.Converters.Add(new DateTimeConverter());
                options.JsonSerializerOptions.Converters.Add(new DecimalConverter());
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "DesafioStone", Version = "v1" });
            });


            services.AddSingleton<IRepositorioInicializavel<RegraAreaAtuacao>, RegraAreaAtuacaoRepositorio>();
            services.AddSingleton<IRepositorioInicializavel<RegraFaixaSalarial>, RegraFaixaSalarialRepositorio>();
            services.AddSingleton<IRepositorioInicializavel<RegraTempoAdmissao>, RegraTempoAdmissaoRepositorio>();
            services.AddSingleton<IRepositorioInicializavel<Funcionario>, FuncionarioRepositorio>();

            services.AddScoped<IRepositorio<RegraAreaAtuacao>, RegraAreaAtuacaoRepositorio>();
            services.AddScoped<IRepositorio<RegraFaixaSalarial>, RegraFaixaSalarialRepositorio>();
            services.AddScoped<IRepositorio<RegraTempoAdmissao>, RegraTempoAdmissaoRepositorio>();
            services.AddScoped<IRepositorio<Funcionario>, FuncionarioRepositorio>();

            services.AddScoped<IServico<RegraAreaAtuacao>, RegraAreaAtuacaoServico>();
            services.AddScoped<IServico<RegraFaixaSalarial>, RegraFaixaSalarialServico>();
            services.AddScoped<IServico<RegraTempoAdmissao>, RegraTempoAdmissaoServico>();
            services.AddScoped<IFuncionarioServico, FuncionarioServico>();
            services.AddScoped<IDistribuicaoServico, DistribuicaoServico>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "DesafioStone v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            Firebase.Connection = ConfigurationExtensions.GetConnectionString(this.Configuration, "DefaultConnection");

            app.ApplicationServices.GetService<IRepositorioInicializavel<RegraAreaAtuacao>>().Inicializar();
            app.ApplicationServices.GetService<IRepositorioInicializavel<RegraFaixaSalarial>>().Inicializar();
            app.ApplicationServices.GetService<IRepositorioInicializavel<RegraTempoAdmissao>>().Inicializar();
            app.ApplicationServices.GetService<IRepositorioInicializavel<Funcionario>>().Inicializar();

    }
    }
}
